### A Simple Classification Problem

### Objective
 This is a programming test that requires at most 3 hours for completion. You can use your programming language of your
 choice. However, our hints and recommendations \(such as choice of framework etc.\), are only given in Java and Python.

### The Program
 The task is to write a program that will have three separate components (or classes) in three different files:

 1. **FlowerTrainer**: a class that trains multi-class classifiers for classifying flowers when provided with
 a training data in the following JSON format:

 ```[json]

 {
    "flowers":["flower_label_1","flower_label_2", ...,"flower_label_n"],
    "feature-space":["feature_1","feature_2",...,"feature_m"],
    "training-records":[
        {
            "label":"flower_label_1",
            "features":[f1,f2,...,fm]
        },
        {
            "label":"flower_label_2",
            "features":[f1,f2,...,fm]
        },
        {
            "label":"flower_label_3",
            "features":[f1,f2,...,fm]
        }, ...
    ]
 }

 ```
 * Requirements:
    * The class must have at least two methods:
        * a method called ```train``` which takes the ```json``` training input as defined above and trains the model.
        * a method called ```save``` which takes a ```string path``` and saves the model associated with
        the object created from this class as serialized object on the file system.

2. **FlowerClassifier**: A class that loads models that are saved by the ```FlowerTrainer```. This class uses
the loaded model to predict the label of a flower when provided with a set of features.

  * Requirements:
    * The class must have at least two methods:
        * a method called ```predict``` which takes a ```json``` in the following format:
        ```[json]

         {
            "input-records":[
                {
                    "id":1,
                    "features":[f1,f2,...,fm]
                },
                {
                    "id":2,
                    "features":[f1,f2,...,fm]
                },
                {
                    "id":3,
                    "features":[f1,f2,...,fm]
                }, ...
            ]
         }

         ```
         And then returns a json as output in the following format:

         ```[json]

                  {
                     "output-records":[
                         {
                             "id":1,
                             "label":"flower_label_1"
                         },
                         {
                             "id":2,
                             "label":"flower_label_3"
                         },
                         {
                             "id":3,
                             "label":"flower_label_1"
                         }, ...
                     ]
                  }

         ```
    * An optional method called ```load``` which loads a model from a given ```string path```. You can alternatively
    initialize objects of this class with a model upon construction depending on any design pattern that you choose to implement.

3. **Main**: a main program that
    * 3.1 loads and transform the training data (see data section below for sample data)
    * 3.2 creates a ```FlowerTrainer``` object to train a model using the training data and then saves that model to a file.
    * 3.3 creates a ```FlowerClassifier``` object to label some input data as described above.


### The Data

To test your program, we recommend using a popular standard data set called [Iris data-set](https://archive.ics.uci.edu/ml/machine-learning-databases/iris)

After transforming the data-set to the ```json``` format defined above, you can use a training set similar to this:

```[json]

 {
   "flowers":["Iris-setosa","Iris-versicolour","Iris-virginica"],
   "feature-space":["sepal_length","sepal_width","petal_length","petal_width"],
   "training-records":[{
     "label":"Iris-setosa",
     "features":[5.1,3.5,1.4,0.2]
   },{
     "label":"Iris-setosa",
     "features":[4.9,3.0,1.4,0.2]
   },{
     "label":"Iris-setosa",
     "features":[4.7,3.2,1.3,0.2]
   } ...

 ```

For your convenience we have provided the sample data as ```iris.json``` in this repo.

### Bonus Question:
 * Is it possible to use Logistic Regression to do multi-class classification? If yes, can you implement Logistic Regression method in your train function?
 * If you were to evaluate your classifier, explain in one paragraph that what kind of metrics you could have provide
 and/or how you would have separate your test from training data.

### Recommendations and Hints:

In general, we recommend that you aim for functionality first and then add TODOs for additional features and/or
tests for the logic of your functions wherever you deem necessary.

In terms of language and frameworks, we recommend the following:

    * If you are using Python:
        * We recommend using [scikit-learn](http://scikit-learn.org), you can use any multi-class classifier that applies
        one and only one label when predicting.
        * We recommend using ```pickle``` to serialize the models or any additional data to the file system.

    * If you are using Java:
        * We recommend using [Weka](http://www.cs.waikato.ac.nz/ml/weka/), you can use any multi-class classifier that
        applies one and only one label when predicting.
        * For model serialization, you can use simple Java serialized object or any other standard method based
        on your choice framework

### Documentation

Please add a simple README.md file to the project with clear instruction on how to run your program. You can also add
any additional documentation such as answer to the Bonus question in the same README file.